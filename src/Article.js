var Article = function($, $article) {
    var that = {};
    that.hide = function() {
        // TODO this causes broken previews in posts (called before some post-parsing)
        $article.closest('li').remove();
    };
    that.getId = function() {
        return $article.data().id;
    };
    that.getTitle = function() {
        return $article.find('h2 a[title]').attr('title');
    };
    that.addRemoveButton = function(removedCallback) {
        var imgUrl = chrome.extension.getURL('src/remove.png');
        var $removeLink = $('<img class="remover" width="24" src="' + imgUrl + '"></img>');
        $removeLink.click(function(e) {
            e.preventDefault();
            that.hide();
            removedCallback({id: that.getId(), title: that.getTitle()});
        });
        // For links
        $article.find('.diggbox').append($removeLink);
        // For posts
        $article.find('.profile').append('<div>').append($removeLink);
    };
    return that;
};