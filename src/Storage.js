var Storage = function ($, onLoaded) {
    var that = this;
    var ids = undefined;
    var save = function() {
        chrome.storage.sync.set({ids: ids});
    };
    var load = function() {
        chrome.storage.sync.get({ids: []}, function(loaded) {
            ids = loaded.ids;
            print();
            onLoaded(that);
        });
    };
    var print = function() {
        console.log(ids);
    };
    load();

    that.add = function(obj) {
        obj && ids.push(obj);
        save();
    };
    that.remove = function(id) {
        var index = -1;
        for (var i = 0; i < ids.length; i++){
            if(ids[i]['id'] == id) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            ids.splice(index, 1);
        }
        save();
    };
    that.getObjects = function() {
        return $.extend({}, ids);
    };
    that.contains = function(id) {
        return $.grep(ids, function(e){ return e.id == id; }).length > 0;
    };
    return that;
};