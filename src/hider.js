$(function($){
    new Storage($, function(storage) {
        $('.article, .wblock').each(function(index, article) {
            var articleObj = new Article($, $(article));
            var id = articleObj.getId();
            if (!id || storage.contains(id)) {
                articleObj.hide();
            } else {
                articleObj.addRemoveButton(function(removed) {
                    storage.add(removed);
                });
            }
        });
    });
});
$.noConflict();