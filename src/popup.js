$(function() {
    var $articles = $('.articles');

    var renderRemoved = function(storage) {
        $articles.empty();
        var articles = storage.getObjects();
        $.each(articles, function(index, article) {
            var $li = $('<li>');
            var $remove = $('<a href="#">Remove</a>').click(function() {
                storage.remove(article.id);
                renderRemoved(storage);
            });
            $li.append(article.title).append(' ').append($remove);
            $articles.append($li);
        });
    };

    var storage = new Storage($, function(storage) {
        renderRemoved(storage);
    });

});